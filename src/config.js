/* jshint node:true */

'use strict';

var fs = require('fs'),
    path = require('path'),
    safe = require('safetydance');

var HOME = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;

exports = module.exports = {
    setActive: setActive,

    set: set,
    get: get,

    cloudronGet: cloudronGet,
    cloudronSet: cloudronSet,

    // appstore
    appStoreToken: function () { return get('appStoreToken'); },
    appStoreOrigin: function () { return get('appStoreOrigin'); },

    // current cloudron
    token: cloudronGet.bind(null, 'token'),
    setToken: cloudronSet.bind(null, 'token'),

    cloudron: cloudronGet.bind(null, 'cloudron'),
    setCloudron: cloudronSet.bind(null, 'cloudron'),

    fqdn: cloudronGet.bind(null, 'fqdn'),
    setFqdn: cloudronSet.bind(null, 'fqdn'),

    provider: cloudronGet.bind(null, 'provider'),
    setProvider: cloudronSet.bind(null, 'provider'),

    apiEndpoint: cloudronGet.bind(null, 'apiEndpoint'),
    setApiEndpoint: cloudronSet.bind(null, 'apiEndpoint'),

    // machine subcommand values
    providerToken: cloudronGet.bind(null, 'providerToken'),
    setProviderToken: cloudronSet.bind(null, 'providerToken'),

    region: cloudronGet.bind(null, 'region'),
    setRegion: cloudronSet.bind(null, 'region'),

    awsRegion: cloudronGet.bind(null, 'awsRegion'),
    setAwsRegion: cloudronSet.bind(null, 'awsRegion'),

    type: cloudronGet.bind(null, 'type'),
    setType: cloudronSet.bind(null, 'type'),

    accessKeyId: cloudronGet.bind(null, 'accessKeyId'),
    setAccessKeyId: cloudronSet.bind(null, 'accessKeyId'),

    secretAccessKey: cloudronGet.bind(null, 'secretAccessKey'),
    setSecretAccessKey: cloudronSet.bind(null, 'secretAccessKey'),

    backupKey: cloudronGet.bind(null, 'backupKey'),
    setBackupKey: cloudronSet.bind(null, 'backupKey'),

    backupBucket: cloudronGet.bind(null, 'backupBucket'),
    setBackupBucket: cloudronSet.bind(null, 'backupBucket'),

    sshKey: cloudronGet.bind(null, 'sshKey'),
    setSshKey: cloudronSet.bind(null, 'sshKey'),

    size: cloudronGet.bind(null, 'size'),
    setSize: cloudronSet.bind(null, 'size'),

    // for testing
    _configFilePath: path.join(HOME, '.cloudron.json')
};

var gConfig = safe.JSON.parse(safe.fs.readFileSync(exports._configFilePath)) || {};

// per default we fetch the last cloudron which a user performed an explicit login
var gCurrent = safe.query(gConfig, 'cloudrons.default') || null;

// precedence: env var, config file, default
if (process.env.APPSTORE_ORIGIN || !get('appStoreOrigin')) set('appStoreOrigin', process.env.APPSTORE_ORIGIN || 'https://api.cloudron.io');

// allow self signed certs if set in config file
if (gConfig.acceptSelfSignedCerts) process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';

// This is mostly called from helper.detectCloudronApiEndpoint() which will select the correct config section then
function setActive(fqdn) {
    gCurrent = fqdn;
}

function save() {
    fs.writeFileSync(exports._configFilePath, JSON.stringify(gConfig, null, 4));
}

function set(key, value) {
    // reload config from disk and set. A parallel cli process could have adjusted values
    gConfig = safe.JSON.parse(safe.fs.readFileSync(exports._configFilePath)) || {};

    safe.set(gConfig, key, value);
    save();
}

function get(key) {
    return safe.query(gConfig, key);
}

function cloudronGet(key) {
    return get('cloudrons.' + gCurrent + '.' + key);
}

function cloudronSet(key, value) {
    return set('cloudrons.' + gCurrent + '.' + key, value);
}
