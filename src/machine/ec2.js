'use strict';

var assert = require('assert'),
    aws = require('./aws.js'),
    config = require('../config.js'),
    ec2tasks = require('./ec2tasks.js'),
    hat = require('hat'),
    helper = require('../helper.js');

exports = module.exports = {
    create: create,
    restore: restore,
    upgrade: upgrade,
    migrate: migrate,
    getBackupListing: getBackupListing
};

function getBackupListing(fqdn, options, callback) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof callback, 'function');

    // select correct config
    config.setActive(fqdn);

    options.region = options.region || config.region();
    options.accessKeyId = options.accessKeyId || config.accessKeyId();
    options.secretAccessKey = options.secretAccessKey || config.secretAccessKey();
    options.backupBucket = options.backupBucket || config.backupBucket();

    if (!options.region) helper.missing('region');
    if (!options.accessKeyId) helper.missing('access-key-id');
    if (!options.secretAccessKey) helper.missing('secret-access-key');
    if (!options.backupBucket) helper.missing('backup-bucket');

    aws.init({
        region: options.region,
        accessKeyId: options.accessKeyId,
        secretAccessKey: options.secretAccessKey
    });

    aws.listBackups(options.backupBucket, fqdn, function (error, result) {
        if (error) return callback(error);

        // update the config
        config.setRegion(options.region);
        config.setAccessKeyId(options.accessKeyId);
        config.setSecretAccessKey(options.secretAccessKey);
        config.setBackupBucket(options.backupBucket);

        callback(null, result);
    });
}

function create(options, version, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof version, 'string');
    assert.strictEqual(typeof callback, 'function');

    if (!options.accessKeyId) helper.missing('access-key-id');
    if (!options.secretAccessKey) helper.missing('secret-access-key');
    if (!options.diskSize) helper.missing('disk-size');
    if (!options.backupBucket) helper.missing('backup-bucket');
    if (!options.sshKey) helper.missing('ssh-key');
    if (!options.type) helper.missing('type');
    if (!options.region) helper.missing('region');

    if (options.diskSize < 30) helper.exit('--disk-size must be at least 30');

    if (!options.subnet ^ !options.securityGroup) return helper.exit('either both --subnet and --security-group must be provided OR none');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    if (!options.backupKey) {
        console.log();
        console.log('No backup key specified.');
        options.backupKey = hat(256);
        console.log('Generated backup key: ', options.backupKey.bold.cyan);
        console.log('The backup key is also stored in the %s config file in your home folder.', 'cloudron.json'.bold.cyan);
        console.log('Remember to keep the backup key stashed. You will need it to restore your Cloudron!'.yellow);
        console.log();
    }

    var params = {
        awsRegion: options.region,
        accessKeyId: options.accessKeyId,
        secretAccessKey: options.secretAccessKey,
        backupKey: options.backupKey,
        backupBucket: options.backupBucket,
        version: version,
        type: options.type,
        sshKey: options.sshKey,
        domain: options.fqdn,
        subnet: options.subnet,
        securityGroup: options.securityGroup,
        size: options.diskSize
    };

    ec2tasks.create(params, function (error) {
        if (error) return callback(error);

        // stash for further use
        config.setActive(options.fqdn);
        config.setProvider('ec2');
        config.setRegion(options.region);
        config.setType(options.type);
        config.setAccessKeyId(options.accessKeyId);
        config.setSecretAccessKey(options.secretAccessKey);
        config.setBackupKey(options.backupKey);
        config.setBackupBucket(options.backupBucket);
        config.setSshKey(options.sshKey);
        config.setSize(options.diskSize);

        callback();
    });
}

function restore(options, backup, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof backup, 'object');
    assert.strictEqual(typeof callback, 'function');

    // select correct config
    config.setActive(options.fqdn);

    options.type = options.type || config.type();
    options.region = options.region || config.region();
    options.diskSize = options.diskSize || config.size();
    options.accessKeyId = options.accessKeyId || config.accessKeyId();
    options.secretAccessKey = options.secretAccessKey || config.secretAccessKey();
    options.backupKey = options.backupKey || config.backupKey();
    options.backupBucket = options.backupBucket || config.backupBucket();
    options.sshKey = options.sshKey || config.sshKey();

    if (!options.type) helper.missing('type');
    if (!options.region) helper.missing('region');
    if (!options.diskSize) helper.missing('disk-size');
    if (!options.accessKeyId) helper.missing('access-key-id');
    if (!options.secretAccessKey) helper.missing('secret-access-key');
    if (!options.backupKey) helper.missing('backup-key');
    if (!options.backupBucket) helper.missing('backup-bucket');
    if (!options.sshKey) helper.missing('ssh-key');

    if (options.diskSize < 30) helper.exit('--disk-size must be at least 30');

    if (!options.subnet ^ !options.securityGroup) return helper.exit('either both --subnet and --security-group must be provided OR none');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    var params = {
        awsRegion: options.region,
        accessKeyId: options.accessKeyId,
        secretAccessKey: options.secretAccessKey,
        backupBucket: options.backupBucket,
        backupKey: options.backupKey,
        backup: backup,
        type: options.type,
        size: options.diskSize,
        sshKey: options.sshKey,
        domain: options.fqdn,
        subnet: options.subnet,
        securityGroup: options.securityGroup
    };

    ec2tasks.restore(params, function (error) {
        if (error) return callback(error);

        // update the config
        config.setType(options.type);
        config.setRegion(options.region);
        config.setSize(options.diskSize);
        config.setAccessKeyId(options.accessKeyId);
        config.setSecretAccessKey(options.secretAccessKey);
        config.setBackupKey(options.backupKey);
        config.setBackupBucket(options.backupBucket);
        config.setSshKey(options.sshKey);

        callback();
    });
}

function upgrade(updateInfo, options, callback) {
    assert.strictEqual(typeof updateInfo, 'object');
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (!options.sshKey) helper.missing('ssh-key');
    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    var params = {
        version: updateInfo.version,
        domain: config.fqdn(),
        sshKey: options.sshKey
    };

    ec2tasks.upgrade(params, callback);
}

function migrate(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof options.fqdn, 'string');
    assert.strictEqual(typeof callback, 'function');

    if (options.region) helper.exit('Moving to another EC2 region is not yet supported');

    options.sshKey = options.sshKey || config.sshKey();
    options.accessKeyId = options.accessKeyId || config.accessKeyId();
    options.secretAccessKey = options.secretAccessKey || config.secretAccessKey();
    options.diskSize = options.diskSize || config.size();

    if (!options.sshKey) helper.missing('ssh-key');
    if (!options.accessKeyId) helper.missing('access-key-id');
    if (!options.secretAccessKey) helper.missing('secret-access-key');

    if (options.diskSize && options.diskSize < 30) helper.exit('--disk-size must be at least 30');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    var params = {
        fqdn: options.fqdn,
        sshKey: options.sshKey,
        accessKeyId: options.accessKeyId,
        secretAccessKey: options.secretAccessKey,
        newFqdn: options.newFqdn || null,
        type: options.type,
        size: options.diskSize
    };

    ec2tasks.migrate(params, callback);
}
