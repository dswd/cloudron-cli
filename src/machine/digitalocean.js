'use strict';

var assert = require('assert'),
    async = require('async'),
    aws = require('./aws.js'),
    config = require('../config.js'),
    fs = require('fs'),
    hat = require('hat'),
    helper = require('../helper.js'),
    path = require('path'),
    readlineSync = require('readline-sync'),
    ssh = require('./ssh.js'),
    safe = require('safetydance'),
    semver = require('semver'),
    superagent = require('superagent'),
    util = require('util');

exports = module.exports = {
    create: create,
    restore: restore,
    upgrade: upgrade,
    migrate: migrate,
    getBackupListing: getBackupListing
};

var IMAGE_SLUG = 'ubuntu-16-04-x64';

function checkBackup(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.backupConfig, 'object');
    assert.strictEqual(typeof params.backupConfig.provider, 'string');
    assert.strictEqual(typeof callback, 'function');

    // skip if filesystem is not used
    if (params.backupConfig.provider !== 'filesystem') return callback();

    console.log('Checking backup...');

    var backupFilePath = path.join(params.backupFolder, params.backup.id);

    fs.stat(backupFilePath, function (error, result) {
        if (error) return callback(util.format('Unable to find backup at %s.', backupFilePath));
        if (!result.isFile()) return callback(util.format('Requested backup is not a file at %s.', backupFilePath));

        // TODO check if encryted or not, the server currently only supports encrypted ones

        callback();
    });
}

function checkDNSZone(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.token, 'string');
    assert.strictEqual(typeof params.domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    function promptForCreation() {
        var answer = readlineSync.question('Domain not found on DigitalOcean, should it be added (y/n)? ');
        if (answer !== 'y') helper.exit();

        var dnsRecords = null;

        function done() {
            console.log('Domain %s created successfully.', params.domain);
            console.log();
            console.log('Please ensure your domain has set the following nameservers:'.yellow);
            dnsRecords.forEach(function (record) {
                if (record.type === 'NS') console.log('  %s'.bold, record.data);
            });
            console.log();

            callback();
        }

        var data = {
            name: params.domain,
            ip_address: '1.2.3.4'
        };

        superagent.post('https://api.digitalocean.com/v2/domains').send(data).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
            if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
            if (result.statusCode === 422) return callback('Creating domain failed. This domain already belongs to another DigitalOcean account');
            if (result.statusCode !== 201) return callback(util.format('Creating domain failed. %s %j', result.statusCode, result.body));

            // now remove the dummy A record - not fatal, all errors are ignored
            superagent.get('https://api.digitalocean.com/v2/domains/' + params.domain + '/records').set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
                if (error) return done();

                dnsRecords = result.body.domain_records;

                var record = dnsRecords.filter(function (record) { return record.type === 'A' && record.name === '@'; })[0];
                if (!record) return done();

                superagent.del('https://api.digitalocean.com/v2/domains/' + params.domain + '/records/' + record.id).set('Authorization', 'Bearer ' + params.token).end(function (error) {
                    done();
                });
            });
        });
    }

    console.log('Checking domain...');

    superagent.get('https://api.digitalocean.com/v2/domains/' + params.domain).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
        if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
        if (result.statusCode === 404) return promptForCreation();
        if (result.statusCode !== 200) return callback(util.format('Looking for domain failed. %s %j', result.statusCode, result.body));

        callback();
    });
}

function checkRegionAndTypeAndImage(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.token, 'string');
    assert.strictEqual(typeof params.region, 'string');
    assert.strictEqual(typeof params.type, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Checking region, type and image...');

    superagent.get('https://api.digitalocean.com/v2/regions').set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
        if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
        if (result.statusCode !== 200) return callback(util.format('Looking for regions failed. %s %j', result.statusCode, result.body));

        var errorMessage;

        var region = result.body.regions.filter(function (region) { return region.slug === params.region; })[0];
        if (!region) {
            errorMessage = util.format('Requested region %s does not exist. Available regions are: %s', params.region, result.body.regions.map(function (r) { return r.slug; }).sort().join(', '));
            return callback(errorMessage);
        }

        var typeExists = region.sizes.some(function (size) { return size === params.type; });
        if (!typeExists) {
            errorMessage = util.format('Requested type %s does not exist in region %s. Available types are: %s', params.type, params.region, region.sizes.join(', '));
            return callback(errorMessage);
        }

        superagent.get('https://api.digitalocean.com/v2/images/' + IMAGE_SLUG).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
            if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
            if (result.statusCode !== 200) return callback(util.format('Looking for image failed. %s %j', result.statusCode, result.body));

            if (!result.body.image.regions.some(function (region) { return region === params.region; })) {
                errorMessage = util.format('Required image does not exist in requested region %s. Available regions are: %s', params.region, result.body.image.regions.sort().join(', '));
                return callback(errorMessage);
            }

            callback();
        });
    });
}

function getSSHKeyId(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.token, 'string');
    assert.strictEqual(typeof params.sshKey, 'string');
    assert.strictEqual(typeof callback, 'function');

    function promptForCreation() {
        console.log('');

        var answer = readlineSync.question('SSH key not found on DigitalOcean, should it be added (y/n)? ');
        if (answer !== 'y') helper.exit();

        var publicKey = helper.getSSHPublicKey(params.sshKey);
        if (!publicKey) return callback('Unable to extract public key portion from %s', params.sshKey);

        var data = {
            name: 'Cloudron',
            public_key: publicKey
        };

        superagent.post('https://api.digitalocean.com/v2/account/keys').send(data).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
            if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
            if (result.statusCode !== 201) return callback(util.format('Adding SSH key failed. %s %j', result.statusCode, result.body));

            params.sshKeyId = result.body.ssh_key.id;

            callback();
        });
    }

    process.stdout.write('Looking up SSH key ID... ');

    var fingerprints = helper.getSSHFingerprint(params.sshKey);
    if (fingerprints.length === 0) return callback('Unable to get fingerprint from ssh key');

    superagent.get('https://api.digitalocean.com/v2/account/keys').set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
        if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
        if (result.statusCode !== 200) return callback(util.format('Looking for SSH key IDs failed. %s %j', result.statusCode, result.body));

        var sshKeys = result.body.ssh_keys || [];

        var potentialKeys = sshKeys.filter(function (key) { return fingerprints.some(function (fingerprint) { return fingerprint === key.fingerprint; }); });
        if (!potentialKeys.length) return promptForCreation();

        params.sshKeyId = potentialKeys[0].id;

        console.log(params.sshKeyId);

        callback();
    });
}

function createServer(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.domain, 'string');
    assert.strictEqual(typeof params.region, 'string');
    assert.strictEqual(typeof params.type, 'string');
    assert.strictEqual(typeof params.sshKeyId, 'number');
    assert.strictEqual(typeof params.token, 'string');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Creating server...');

    ssh.getUserData('digitalocean', params, function (error, result) {
        if (error) return callback(error);

        var data = {
            name: 'my.' + params.domain,
            region: params.region,
            size: params.type,
            image: IMAGE_SLUG,
            ssh_keys: [ params.sshKeyId ],
            backups: false
        };

        // only set this if version is based on meta data api
        if (semver.lte(params.version, '0.21.1')) data.user_data = JSON.stringify(result);

        superagent.post('https://api.digitalocean.com/v2/droplets').send(data).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
            if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
            if (result.statusCode !== 202) return callback(util.format('Droplet creation failed. %s %j', result.statusCode, result.body));

            params.instanceId = result.body.droplet.id;
            params.createAction = result.body.links.actions[0];

            console.log(params.instanceId);

            callback();
        });
    });
}

function rebuildServer(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.oldInstanceId, 'number');
    assert.strictEqual(typeof params.token, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Rebuilding server...');

    var data = {
        type: 'rebuild',
        image: IMAGE_SLUG
    };

    superagent.post('https://api.digitalocean.com/v2/droplets/' + params.oldInstanceId + '/actions').send(data).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
        if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
        if (result.statusCode !== 201) return callback(util.format('Droplet rebuild failed. %s %j', result.statusCode, result.body));

        params.instanceId = params.oldInstanceId;
        params.rebuildAction = result.body.action;

        callback();
    });
}

function waitForServer(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.token, 'string');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Waiting for server to come up...');

    var actionUrl;

    if (params.createAction) {
        actionUrl = params.createAction.href;
    } else if (params.rebuildAction) {
        assert.strictEqual(typeof params.instanceId, 'number');
        actionUrl = 'https://api.digitalocean.com/v2/droplets/' + params.instanceId + '/actions/' + params.rebuildAction.id;
    } else {
        return callback('Unknown droplet action');
    }

    async.forever(function (callback) {
        superagent.get(actionUrl).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
            if (error && !error.response) return callback();
            if (result.statusCode !== 200) return callback(util.format('Waiting for droplet failed. %s %j', result.statusCode, result.body));

            if (result.body.action.status !== 'completed') {
                process.stdout.write('.');
                setTimeout(callback, 2000);
                return;
            }

            callback('done');
        });
    }, function (errorOrDone) {
        if (errorOrDone !== 'done') return callback(errorOrDone);

        process.stdout.write('\n');

        callback();
    });
}

function getIp(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.token, 'string');
    assert.strictEqual(typeof params.instanceId, 'number');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Getting public IP...');

    superagent.get('https://api.digitalocean.com/v2/droplets/' + params.instanceId).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
        if (error && !error.response) return callback(error.message);
        if (result.statusCode !== 200) return callback(util.format('Droplet details failed. %s %j', result.statusCode, result.body));

        params.publicIP = result.body.droplet.networks.v4[0].ip_address;

        console.log(params.publicIP);

        callback();
    });
}

function deleteOldInstance(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.oldInstanceId, 'number');
    assert.strictEqual(typeof callback, 'function');

    console.log('Cleanup old instance %s...', params.oldInstanceId);

    superagent.del('https://api.digitalocean.com/v2/droplets/' + params.oldInstanceId).set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
        if (error && !error.response) return callback(error.message);
        if (result.statusCode !== 204) return callback(util.format('Failed to cleanup old droplet. %s %j', result.statusCode, result.body));

        callback();
    });
}

function getInstanceResources(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    helper.superagentEnd(function () {
        return superagent.get(helper.createUrl('/api/v1/settings/backup_config')).query({ access_token: config.token() });
    }, function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode !== 200) return callback(new Error('Failed to get instance details.'));

        params.backupConfig = result.body;

        // TODO this only supports up to 200 droplets in this account
        superagent.get('https://api.digitalocean.com/v2/droplets?page=1&per_page=200').set('Authorization', 'Bearer ' + params.token).end(function (error, result) {
            if (error && !error.response) return callback((result && result.body) ? result.body.message : error.message);
            if (result.statusCode !== 200) return callback(util.format('Looking for droplet failed. %s %j', result.statusCode, result.body));

            var droplets = result.body.droplets || [];
            var dropletName = 'my.' + params.domain;

            var droplet = droplets.filter(function (droplet) { return droplet.name === dropletName; })[0];
            if (!droplet) return callback('No droplet found with the name ' + dropletName);

            params.oldInstanceId = droplet.id;
            params.type = droplet.size_slug;
            params.region = droplet.region.slug;

            callback();
        });
    });
}

function warnUserIfFilesystemBackendIsUsed(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (params.backupConfig.provider !== 'filesystem') return callback();

    if (!params.skipQuestions) {
        console.log();
        console.log('Your Cloudron uses the local filesystem for backups! This can easily result in data loss.'.red);
        console.log('Before you proceed, please download the latest backup manually to be able to restore in case:'.red);
        console.log();
        console.log('  cloudron machine backup download'.bold);
        console.log();
        console.log('Also ensure you have the backup encryption key stored in a safe location. Your key is:'.red);
        console.log('Backup Encryption Key: ', params.backupConfig.key.bold);
        console.log();

        var answer = readlineSync.question('I have done that, continue now (y/n)? ');
        if (answer !== 'y') return helper.exit();
    } else {
        console.log();
        console.log('Backup Encryption Key: ', params.backupConfig.key.bold);
        console.log();
    }

    callback();
}

// ----------------------------------------------------------------------------
//   Actions
// ----------------------------------------------------------------------------

function getBackupListing(fqdn, options, callback) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (!options.backupProvider) helper.missing('backup-provider');

    // TODO ideally we would share this code with ec2.js
    if (options.backupProvider === 's3') {
        if (!options.awsBackupBucket) helper.missing('aws-backup-bucket');
        if (!options.awsAccessKeyId) helper.missing('aws-access-key-id');
        if (!options.awsSecretAccessKey) helper.missing('aws-secret-access-key');
        if (!options.awsRegion) helper.missing('aws-region');
        if (typeof options.awsBackupPrefix !== 'string') helper.missing('aws-backup-prefix');

        aws.init({
            region: options.awsRegion,
            accessKeyId: options.awsAccessKeyId,
            secretAccessKey: options.awsSecretAccessKey
        });

        aws.listBackups(options.awsBackupBucket, options.awsBackupPrefix, function (error, result) {
            if (error) return callback(error);

            // TODO update the config

            callback(null, result);
        });
    } else if (options.backupProvider === 'filesystem') {
        if (!options.backupFolder) helper.missing('backup-folder');

        options.backupFolder = path.resolve(options.backupFolder);

        var stats = safe.fs.statSync(options.backupFolder);
        if (!stats) helper.exit('Provided backup folder does not exist');
        if (!stats.isDirectory()) helper.exit('Provided backup folder is not a folder');

        var entries = safe.fs.readdirSync(options.backupFolder);
        if (!entries) helper.exit('Unable to list backup folder content');

        var results = entries.map(function (entry) {
            var filePath = path.join(options.backupFolder, entry);

            var match = filePath.match(/\/backup_(.*)-v(.*).tar.gz$/);
            if (!match) return null;

            var date = new Date(match[1]);
            if (date.toString() === 'Invalid Date') return null;

            return {
                id: entry,
                creationTime: date.toISOString(),
                version: match[2],
                filename: entry
            };
        }).filter(function (backup) { return !! backup; });

        callback(null, results);
    } else {
        callback('Backup provider ' + options.backupProvider + ' not supported');
    }
}

function create(options, version, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof version, 'string');
    assert.strictEqual(typeof callback, 'function');

    if (!options.token) helper.missing('token');
    if (!options.region) helper.missing('region');
    if (!options.sshKey) helper.missing('ssh-key');
    if (!options.type) helper.missing('type');
    if (!options.region) helper.missing('region');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    if (!options.backupKey) {
        console.log();
        console.log('No backup key specified.');
        options.backupKey = hat(256);
        console.log('Generated backup key: ', options.backupKey.bold.cyan);
        console.log('The backup key is also stored in the %s config file in your home folder.', 'cloudron.json'.bold.cyan);
        console.log('Remember to keep the backup key stashed. You will need it to restore your Cloudron!'.yellow);
        console.log();
    }

    var params = {
        token: options.token,
        region: options.region,
        backupKey: options.backupKey,
        version: version,
        type: options.type,
        sshKey: options.sshKey,
        domain: options.fqdn
    };

    console.log('Using version %s', version.cyan.bold);

    var tasks = [
        checkRegionAndTypeAndImage.bind(null, params),
        checkDNSZone.bind(null, params),
        getSSHKeyId.bind(null, params),
        createServer.bind(null, params),
        waitForServer.bind(null, params),
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'digitalocean', params),
        ssh.uploadUserData.bind(null, 'digitalocean', params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'digitalocean', params)
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        // stash for further use
        config.setActive(options.fqdn);
        config.setProvider('digitalocean');
        config.setProviderToken(options.token);
        config.setRegion(options.region);
        config.setType(options.type);
        config.setBackupKey(options.backupKey);
        config.setSshKey(options.sshKey);

        console.log('');
        console.log('Cloudron created with:');
        console.log('  ID:        %s', String(params.instanceId).cyan);
        console.log('  Public IP: %s', params.publicIP.cyan);
        console.log('');

        callback();
    });
}

function restore(options, backup, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof backup, 'object');
    assert.strictEqual(typeof callback, 'function');

    // select correct config
    config.setActive(options.fqdn);

    options.token = options.token || config.providerToken();
    options.type = options.type || config.type();
    options.region = options.region || config.region();
    options.backupKey = options.backupKey || config.backupKey();
    options.sshKey = options.sshKey || config.sshKey();

    if (!options.token) helper.missing('token');
    if (!options.type) helper.missing('type');
    if (!options.region) helper.missing('region');
    if (!options.backupKey) helper.missing('backup-key');
    if (!options.sshKey) helper.missing('ssh-key');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    var params = options;

    // translate properties
    params.domain = options.fqdn;
    params.backup = backup;

    // use the version of the backup
    params.version = options.backup.version;

    if (options.backupProvider === 'filesystem') {
        params.backupConfig = {
            provider: 'filesystem',
            backupFolder: '/var/backups',
            key: options.backupKey
        };
    } else if (options.backupProvider === 's3') {
        params.backupConfig = {
            provider: 's3',
            key: options.backupKey,
            region: options.awsRegion,
            bucket: options.awsBackupBucket,
            prefix: options.awsBackupPrefix,
            accessKeyId: options.awsAccessKeyId,
            secretAccessKey: options.awsSecretAccessKey
        };
    } else {
        return callback('Backup provider ' + options.backupProvider + ' not supported');
    }

    console.log('Restoring %s to backup %s with version %s', params.domain.cyan.bold, params.backup.id.cyan.bold, params.backup.version.cyan.bold);

    var tasks = [
        checkRegionAndTypeAndImage.bind(null, params),
        checkBackup.bind(null, params),
        checkDNSZone.bind(null, params),
        getSSHKeyId.bind(null, params),
        ssh.getRestoreDetails.bind(null, params),
        createServer.bind(null, params),
        waitForServer.bind(null, params),
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'digitalocean', params),
        ssh.uploadUserData.bind(null, 'digitalocean', params),
        ssh.uploadBackupFiles.bind(null, params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'digitalocean', params)
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        // update the config
        config.setType(options.type);
        config.setRegion(options.region);
        config.setProviderToken(options.token);
        config.setBackupKey(options.backupKey);
        config.setSshKey(options.sshKey);

        console.log('');
        console.log('Cloudron created with:');
        console.log('  ID:        %s', String(params.instanceId).cyan);
        console.log('  Public IP: %s', params.publicIP.cyan);
        console.log('');

        callback();
    });
}

function upgrade(updateInfo, options, callback) {
    assert.strictEqual(typeof updateInfo, 'object');
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof callback, 'function');

    options.token = options.token || config.providerToken();

    if (!options.token) helper.missing('token');
    if (!options.sshKey) helper.missing('ssh-key');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    var params = {
        version: updateInfo.version,
        domain: config.fqdn(),
        sshKey: options.sshKey,
        token: options.token,
        skipQuestions: !!options.yes
    };

    var tasks = [
        getInstanceResources.bind(null, params),
        warnUserIfFilesystemBackendIsUsed.bind(null, params),
        checkRegionAndTypeAndImage.bind(null, params),
        ssh.verifySshAccess.bind(null, params),
        getSSHKeyId.bind(null, params),
        helper.createCloudronBackup,
        ssh.getLastBackup.bind(null, params),
        ssh.getRestoreDetails.bind(null, params),
        ssh.downloadBackupFiles.bind(null, params),
        rebuildServer.bind(null, params),
        waitForServer.bind(null, params),
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'digitalocean', params),
        ssh.uploadUserData.bind(null, 'digitalocean', params),
        ssh.uploadBackupFiles.bind(null, params),
        ssh.cleanupBackupFiles.bind(null, params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'digitalocean', params)
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        // update the config
        config.setProviderToken(options.token);
        config.setSshKey(options.sshKey);

        callback();
    });
}

function migrate(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof options.fqdn, 'string');
    assert.strictEqual(typeof callback, 'function');

    // select correct config
    config.setActive(options.fqdn);

    options.token = options.token || config.providerToken();
    options.sshKey = options.sshKey || config.sshKey();

    if (!options.sshKey) helper.missing('ssh-key');
    if (!options.token) helper.missing('token');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    console.log('Migrating %s...', options.fqdn.cyan.bold);
    if (options.newFqdn) console.log('  New Domain: %s', options.newFqdn.yellow);
    if (options.type) console.log('  New Instance Type: %s', options.type.yellow);
    if (options.region) console.log('  New Region: %s', options.region.yellow);

    var params = {
        domain: options.fqdn,
        newDomain: options.newFqdn || null,
        sshKey: options.sshKey,
        token: options.token,
        type: options.type || null,
        region: options.region || null,
        skipQuestions: !!options.yes
    };

    var tasks = [
        ssh.getInstanceVersion.bind(null, params),
        getInstanceResources.bind(null, params),
        warnUserIfFilesystemBackendIsUsed.bind(null, params),
        checkRegionAndTypeAndImage.bind(null, params),
        ssh.verifySshAccess.bind(null, params),
        getSSHKeyId.bind(null, params),
        helper.createCloudronBackup,
        ssh.getLastBackup.bind(null, params),
        ssh.getRestoreDetails.bind(null, params),
        ssh.downloadBackupFiles.bind(null, params),
        function overwriteCloudronDetails(callback) {
            if (options.type) params.type = options.type;
            if (options.region) params.region = options.region;
            if (options.newFqdn) params.domain = options.newFqdn;

            callback(null);
        },
        ssh.retireOldCloudron.bind(null, params),
        createServer.bind(null, params),
        waitForServer.bind(null, params),
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'digitalocean', params),
        ssh.uploadUserData.bind(null, 'digitalocean', params),
        ssh.uploadBackupFiles.bind(null, params),
        ssh.cleanupBackupFiles.bind(null, params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'digitalocean', params),
        deleteOldInstance.bind(null, params),
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        // update the config
        config.setType(options.type);
        config.setRegion(options.region);
        config.setProviderToken(options.token);
        config.setSshKey(options.sshKey);

        console.log('');
        console.log('Cloudron created with:');
        console.log('  ID:        %s', String(params.instanceId).cyan);
        console.log('  Public IP: %s', params.publicIP.cyan);
        console.log('');

        callback();
    });
}
