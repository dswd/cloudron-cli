'use strict';

// ********************************************
//
//  This file contains all the code which can
//  be shared for all ssh based provisioning
//  methods.
//
// ********************************************

var assert = require('assert'),
    async = require('async'),
    aws = require('./aws.js'),
    config = require('../config.js'),
    debug = require('debug')('cloudron:ssh'),
    dns = require('dns'),
    fs = require('fs'),
    helper = require('../helper.js'),
    nativeDNS = require('native-dns'),
    os = require('os'),
    path = require('path'),
    rimraf = require('rimraf'),
    semver = require('semver'),
    superagent = require('superagent'),
    tld = require('tldjs'),
    util = require('util'),
    versions = require('./versions.js');

exports = module.exports = {
    initBaseSystem: initBaseSystem,
    rebootServer: rebootServer,
    getUserData: getUserData,
    waitForDNS: waitForDNS,
    waitForStatus: waitForStatus,
    getInstanceVersion: getInstanceVersion,
    verifySshAccess: verifySshAccess,
    getLastBackup: getLastBackup,
    retireOldCloudron: retireOldCloudron,
    uploadUserData: uploadUserData,
    uploadBackupFiles: uploadBackupFiles,
    cleanupBackupFiles: cleanupBackupFiles,
    getRestoreDetails: getRestoreDetails,
    downloadBackupFiles: downloadBackupFiles,
    updateOrUpgrade: updateOrUpgrade
};

function extractBaseImageScript(imageRevision, callback) {
    assert.strictEqual(typeof imageRevision, 'string');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Fetching base image init script...');

    var initScript = 'initializeBaseUbuntuImage.sh';
    var outputFilePath = path.join(os.tmpdir(), initScript);

    superagent.get('https://git.cloudron.io/cloudron/box/raw/' + imageRevision + '/baseimage/' + initScript).redirects(0).timeout(30000).end(function (error, result) {
        if (error || result.statusCode !== 200) return callback(util.format('Error fetching initialize script : %j', error));

        fs.writeFileSync(outputFilePath, result.text);

        callback(null, outputFilePath);
    });
}

function initBaseSystem(provider, params, callback) {
    assert.strictEqual(typeof provider, 'string');
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.publicIP, 'string');
    assert.strictEqual(typeof params.sshKey, 'string');
    assert.strictEqual(typeof params.version, 'string');
    assert.strictEqual(typeof callback, 'function');

    function exec(input, callback) {
        var args = input.split(' ');
        var cmd = args.shift();
        var retries = 10;

        helper.exec(cmd, args, function (error) {
            // retry for some time
            if (error && retries) {
                --retries;
                setTimeout(exec.bind(null, input, callback), 1000);
                return;
            }
            if (error) return callback(error);

            callback();
        });
    }

    versions.details(params.version, function (error, result) {
        if (error) return callback(error);

        // TODO this is already done I think
        var sshKeyFile = helper.findSSHKey(params.sshKey);

        var imageRev = result.imageName.match(/box-(prod|staging|dev)-([\w]+)-.*/)[2];

        extractBaseImageScript(imageRev, function (error, initScriptFilePath) {
            if (error) return callback(error);

            var tasks = [];

            // for ec2 we have to enable root ssh access first
            if (provider === 'ec2') tasks.push(exec.bind(null, 'ssh ubuntu@' + params.publicIP + ' -tt -p 22 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + sshKeyFile + ' sudo sed -e "s/.* \\(ssh-rsa.*\\)/\\1/" -i /root/.ssh/authorized_keys'));

            tasks.push(exec.bind(null, 'scp -P 22 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + sshKeyFile + ' ' + initScriptFilePath + ' root@' + params.publicIP + ':.'));
            tasks.push(exec.bind(null, 'ssh root@' + params.publicIP + ' -tt -p 22 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + sshKeyFile + ' curl ' + result.sourceTarballUrl + ' -o /tmp/box.tar.gz'));
            tasks.push(exec.bind(null, 'ssh root@' + params.publicIP + ' -tt -p 22 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + sshKeyFile + ' /bin/bash /root/initializeBaseUbuntuImage.sh 1337 ' + provider));

            // TODO set revision
            async.series(tasks, function (error) {
                if (error) return callback('Initializing base image failed. ' + error);

                callback();
            });
        });
    });
}

function rebootServer(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.publicIP, 'string');
    assert.strictEqual(typeof params.sshKey, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Rebooting server...');

    helper.exec('ssh', String('root@' + params.publicIP + ' -tt -p 202 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + params.sshKey + ' systemctl reboot').split(' '), function () {
        // ignore errors
        callback();
    });
}

function getUserData(provider, params, callback) {
    assert.strictEqual(typeof provider, 'string');
    assert.strictEqual(typeof params.version, 'string');
    assert.strictEqual(typeof params.domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    versions.details(params.version, function (error, result) {
        if (error) return callback(error);

        helper.createCertificate(params.domain,  function (error, tlsKey, tlsCert) {
            if (error) return callback(error);

            var dnsConfig = {};

            if (provider === 'digitalocean') {
                dnsConfig.provider = 'digitalocean';
                dnsConfig.token = params.token;

                if (!params.backupConfig) {
                    params.backupConfig = {
                        provider: 'filesystem',
                        backupFolder: '/var/backups',
                        key: params.backupKey
                    };
                }
            } else if (provider === 'ec2') {
                dnsConfig.provider = 'route53';
                dnsConfig.accessKeyId = params.accessKeyId;
                dnsConfig.secretAccessKey =params.secretAccessKey;

                if (!params.backupConfig) {
                    params.backupConfig = {
                        provider: 's3',
                        key: params.backupKey,
                        region: params.awsRegion,
                        bucket: params.backupBucket,
                        prefix: params.domain,
                        accessKeyId: params.accessKeyId,
                        secretAccessKey: params.secretAccessKey
                    };
                }
            } else if (provider === 'generic') {
                dnsConfig.provider = 'noop';

                if (!params.backupConfig) {
                    params.backupConfig = {
                        provider: 'filesystem',
                        key: params.backupKey,
                        backupFolder: '/var/backups'
                    };
                }
            } else {
                assert(false, 'Unimplemented provider:' + provider);
            }

            var data = {
                // installer data
                sourceTarballUrl: result.sourceTarballUrl,

                data: {
                    fqdn: params.domain,
                    isCustomDomain: true,
                    version: params.version,
                    boxVersionsUrl: versions.versionsUrl,
                    provider: provider,

                    appstore: {
                        token: '',
                        apiServerOrigin: config.appStoreOrigin()
                    },
                    caas: null,
                    tlsConfig: {
                        provider: process.env.CLOUDRON_TLS_PROVIDER || 'letsencrypt-prod',
                    },
                    tlsCert: tlsCert,
                    tlsKey: tlsKey,

                    appBundle: [], // default app list

                    // obsolete
                    token: '',
                    apiServerOrigin: config.appStoreOrigin(),
                    webServerOrigin: 'https://cloudron.io',

                    restore: {
                        url: params.restoreDetails ? params.restoreDetails.url : null,
                        key: params.restoreDetails ? params.restoreDetails.key : null
                    },
                    backupConfig: params.backupConfig,
                    dnsConfig: dnsConfig,
                    updateConfig: { prerelease: process.env.CLOUDRON_PRERELEASE ? true : false }
                }
            };

            debug('Using user data:', data);

            callback(null, data);
        });
    });
}

function isChangeSynced(fqdn, publicIP, nameserver, callback) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof publicIP, 'string');
    assert.strictEqual(typeof nameserver, 'string');
    assert.strictEqual(typeof callback, 'function');

    // ns records cannot have cname
    dns.resolve4(nameserver, function (error, nsIps) {
        if (error || !nsIps || nsIps.length === 0) return callback(false);

        // the first arg to callback is not an error argument; this is required for async.every
        async.every(nsIps, function (nsIp, iteratorCallback) {
            var req = nativeDNS.Request({
                question: nativeDNS.Question({ name: fqdn, type: 'A' }),
                server: { address: nsIp },
                timeout: 5000
            });

            req.on('end', function () {});
            req.on('error', function () { iteratorCallback(false); });
            req.on('cancelled', function () { iteratorCallback(false); });
            req.on('timeout', function () { return iteratorCallback(false); });

            req.on('message', function (error, message) {
                if (error || !message.answer || message.answer.length === 0) return iteratorCallback(false);
                if (message.answer[0].address !== publicIP) return iteratorCallback(false);

                iteratorCallback(true); // done
            });

            req.send();
        }, callback);
    });
}

// check if IP change has propagated to every nameserver
function waitForDNS(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.domain, 'string');
    assert.strictEqual(typeof params.publicIP, 'string');
    assert.strictEqual(typeof callback, 'function');

    var adminFqdn = 'my.' + params.domain;

    process.stdout.write('Waiting for DNS (may take very long)...');

    async.forever(function (callback) {
        var topLevelDomain = tld.getDomain(params.domain);

        dns.resolveNs(topLevelDomain, function (error, nameservers) {
            if (error) return setTimeout(callback, 5000);
            if (!nameservers) return callback(new Error('Unable to get nameservers'));

            async.every(nameservers, isChangeSynced.bind(null, adminFqdn, params.publicIP), function (synced) {
                process.stdout.write('.');

                // try again if not synced
                setTimeout(function () { callback(synced ? 'done' : null); }, 5000);
            });
        });
    }, function (errorOrDone) {
        if (errorOrDone !== 'done') return callback(errorOrDone);

        process.stdout.write('\n');

        callback();
    });
}

function waitForStatus(provider, params, callback) {
    assert.strictEqual(typeof provider, 'string');
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Waiting for Cloudron to be ready...');

    async.forever(function (callback) {
        superagent.get('https://my.' + params.domain + '/api/v1/cloudron/status').redirects(0).timeout(10000).end(function (error, result) {
            if (!error && result.statusCode === 200 && result.body.provider === provider) return callback('done');

            process.stdout.write('.');

            setTimeout(function () { callback(null); }, 1000);
        });
    }, function (errorOrDone) {
        if (errorOrDone !== 'done') return callback(errorOrDone);

        process.stdout.write('\n');

        callback();
    });
}

function getInstanceVersion(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    helper.superagentEnd(function () {
        return superagent.get(helper.createUrl('/api/v1/cloudron/config')).query({ access_token: config.token() });
    }, function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode !== 200) return callback(new Error('Failed to get instance details. ' + result.statusCode));

        params.version = result.body.version;

        debug('getInstanceVersion: got version', params.version);

        callback();
    });
}

function verifySshAccess(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.sshKey, 'string');
    assert.strictEqual(typeof callback, 'function');

    helper.exec('ssh', helper.getSSH(config.apiEndpoint(), params.sshKey, ' exit 0'), function (error) {
        if (error) return callback('Wrong SSH key provided.');

        callback();
    });
}

function getLastBackup(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    helper.getCloudronBackupList(function (error, result) {
        if (error) return callback(error);

        params.backup = result[0];

        debug('getLastBackup:', params.backup);

        callback();
    });
}

function retireOldCloudron(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.sshKey, 'string');
    assert.strictEqual(typeof callback, 'function');

    helper.exec('ssh', helper.getSSH(config.apiEndpoint(), params.sshKey, ' curl --fail -X POST http://127.0.0.1:3001/api/v1/retire'), function (error) {
        if (error) return callback('Retire failed: ' + error);

        callback();
    });
}

function uploadUserData(provider, params, callback) {
    assert.strictEqual(typeof provider, 'string');
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.version, 'string');
    assert.strictEqual(typeof params.sshKey, 'string');
    assert.strictEqual(typeof params.publicIP, 'string');
    assert.strictEqual(typeof callback, 'function');

    // skip this if version is still based on metadata api
    if (semver.lte(params.version, '0.21.1')) return callback(null);

    console.log('Uploading userdata...');

    getUserData(provider, params, function (error, result) {
        if (error) return callback(error);

        var TMP_FILE = '/tmp/userdata.json';
        fs.writeFileSync(TMP_FILE, JSON.stringify(result, null, 4));

        helper.exec('scp', String('-P 202 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + params.sshKey + ' ' + TMP_FILE + ' root@' + params.publicIP + ':/root/userdata.json').split(' '), function (error){
            if (error) return callback(error);

            rimraf(TMP_FILE, callback);
        });
    });
}

function uploadBackupFiles(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.backupConfig, 'object');
    assert.strictEqual(typeof params.backupConfig.provider, 'string');
    assert.strictEqual(typeof params.backup, 'object');
    assert.strictEqual(typeof params.backup.id, 'string');
    assert.strictEqual(typeof params.sshKey, 'string');
    assert.strictEqual(typeof params.publicIP, 'string');
    assert.strictEqual(typeof callback, 'function');

    // skip if filesystem is not used
    if (params.backupConfig.provider !== 'filesystem') return callback();

    assert.strictEqual(typeof params.backupFolder, 'string');

    console.log('Uploading backup files...');

    fs.readdir(params.backupFolder, function (error, result) {
        if (error) return callback(error);

        // FIXME currently we can't detect which app backup belongs to which box backup :-/
        var appBackupFiles = result.filter(function (file) { return file.indexOf('appbackup_') === 0; });

        appBackupFiles.push(params.backup.id);

        // ensure /var/backups exists
        helper.exec('ssh', String('root@' + params.publicIP + ' -tt -p 202 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + params.sshKey + ' mkdir -p /var/backups').split(' '));

        async.eachSeries(appBackupFiles, function (backupFile, callback) {
            console.log(' -> ', backupFile);
            helper.exec('scp', String('-P 202 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10 -i ' + params.sshKey + ' ' + path.join(params.backupFolder, backupFile) + ' root@' + params.publicIP + ':/var/backups/').split(' '), callback);
        }, callback);
    });
}

function cleanupBackupFiles(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.backupConfig, 'object');
    assert.strictEqual(typeof params.backupConfig.provider, 'string');
    assert.strictEqual(typeof callback, 'function');

    // skip if filesystem is not used
    if (params.backupConfig.provider !== 'filesystem') return callback();

    rimraf(params.backupFolder, callback);
}

function getRestoreDetails(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.backupConfig, 'object');
    assert.strictEqual(typeof params.backupConfig.provider, 'string');
    assert.strictEqual(typeof params.backup, 'object');
    assert.strictEqual(typeof params.backup.id, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Getting restore details...');

    if (params.backupConfig.provider === 's3') {
        aws.init({
            region: params.backupConfig.awsRegion,
            accessKeyId: params.backupConfig.accessKeyId,
            secretAccessKey: params.backupConfig.secretAccessKey
        });

        aws.getBackupUrl(params.backupConfig.bucket, params.domain, params.backup.id, function (error, result) {
            if (error) return callback(error);

            params.restoreDetails = {
                key: params.backupConfig.key,
                url: result
            };

            callback();
        });
    } else if (params.backupConfig.provider === 'filesystem') {
        params.restoreDetails = {
            key: params.backupConfig.key,
            url: 'file://' + path.join('/var/backups', params.backup.id)
        };

        callback();
    } else {
        return callback('Backup provider ' + params.backupConfig.provider + ' not supported');
    }
}

function downloadBackupFiles(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof params.backupConfig, 'object');
    assert.strictEqual(typeof params.backupConfig.provider, 'string');
    assert.strictEqual(typeof params.backup, 'object');
    assert.strictEqual(typeof params.backup.id, 'string');
    assert.strictEqual(typeof callback, 'function');

    // skip for non filesystem backend
    if (params.backupConfig.provider !== 'filesystem') return callback();

    // use current directory for backups as /tmp might be too small
    params.backupFolder = path.join(process.cwd(), '.cloudron_backup');

    helper.downloadBackup(params.backup.id, params.backupFolder, false, callback);
}

function updateOrUpgrade(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof config.apiEndpoint(), 'string');
    assert.strictEqual(typeof callback, 'function');

    if (!options.sshKey) helper.missing('ssh-key');
    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    var port = options.sshPort || (config.provider() === 'caas' ? 202 : 22);

    console.log('Trigger update on %s via ssh'.bold, config.apiEndpoint().cyan);
    helper.exec('ssh', helper.getSSH(config.apiEndpoint(), options.sshKey, ' curl --fail -X POST http://127.0.0.1:3001/api/v1/update', port), callback);
}
