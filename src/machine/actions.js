'use strict';

var assert = require('assert'),
    admin = require('./admin.js'),
    caas = require('./caas.js'),
    config = require('../config.js'),
    digitalocean = require('./digitalocean.js'),
    ec2 = require('./ec2.js'),
    execSync = require('child_process').execSync,
    fs = require('fs'),
    generic = require('./generic.js'),
    helper = require('../helper.js'),
    os = require('os'),
    path = require('path'),
    ssh = require('./ssh.js'),
    superagent = require('superagent'),
    Table = require('easy-table'),
    util = require('util'),
    versions = require('./versions.js');

exports = module.exports = {
    create: create,
    restore: restore,
    migrate: migrate,
    listBackups: listBackups,
    createBackup: createBackup,
    downloadBackup: downloadBackup,
    eventlog: eventlog,
    info: info,
    logs: logs,
    ssh: sshLogin,
    hotfix: hotfix,
    update: update
};

function create(options) {
    if (typeof options !== 'object') return arguments[arguments.length-1].outputHelp();

    if (!options.release) helper.missing('release');
    if (!options.fqdn) helper.missing('fqdn');

    var error = helper.validateFqdn(options.fqdn);
    if (error) return helper.exit(error);

    versions.resolve(options.release, function (error, result) {
        if (error) helper.exit(error);

        var func;
        if (options._name === 'ec2') func = ec2.create;
        else if (options._name === 'caas') func = caas.create;
        else if (options._name === 'digitalocean') func = digitalocean.create;
        else if (options._name === 'generic') func = generic.create;
        else helper.exit('unknown provider"');

        func(options, result, function (error) {
            if (error) helper.exit(error);

            console.log('');
            console.log('Done.'.green, 'You can now setup your Cloudron at ', String('https://my.' + options.fqdn).bold);
            console.log('');

            helper.exit();
        });
    });
}

function restore(options) {
    if (typeof options !== 'object') return arguments[arguments.length-1].outputHelp();

    var api;
    if (options._name === 'ec2') api = ec2;
    else if (options._name === 'caas') api = caas;
    else if (options._name === 'digitalocean') api = digitalocean;
    else helper.exit('unknown provider');

    if (!options.fqdn) helper.missing('fqdn');
    if (!options.backup) helper.missing('backup');

    // we can't detect the my domain as the cloudron might not be on
    if (options.fqdn.indexOf('my.') === 0 || options.fqdn.indexOf('my-') === 0) options.fqdn = options.fqdn.slice(3);

    api.getBackupListing(options.fqdn, options, function (error, result) {
        if (error) helper.exit(error);

        if (result.length === 0) helper.exit('No backups found for %s. Create one first to restore to.', options.fqdn.bold);

        var backupTo = null;
        if (options.backup === 'latest') {
            backupTo = result.sort(function (a, b) { return (new Date(b.creationTime).getTime()) - (new Date(a.creationTime).getTime()); })[0];
            console.log('Using latest backup from %s with version %s', backupTo.creationTime.cyan, backupTo.version.cyan);
        } else {
            backupTo = result.filter(function (b) { return b.id === options.backup; })[0];
            if (!backupTo) helper.exit('Unable to find backup ' + options.backup + '.');
        }

        api.restore(options, backupTo, function (error) {
            if (error) helper.exit(error);

            console.log('');
            console.log('Done.'.green, 'You can now use your Cloudron at ', String('https://my.' + options.fqdn).bold);
            console.log('');

            helper.exit();
        });
    });
}

function migrate(options) {
    if (typeof options !== 'object') return arguments[arguments.length-1].outputHelp();

    var api;
    if (options._name === 'ec2') api = ec2;
    else if (options._name === 'caas') api = caas;
    else if (options._name === 'digitalocean') api = digitalocean;
    else helper.exit('unknown provider');

    if (!options.fqdn) helper.missing('fqdn');

    helper.detectCloudronApiEndpoint(options.fqdn, function (error) {
        if (error) helper.exit(error);

        // Use the real fqdn as my.* could have been passed in
        options.fqdn = config.fqdn();

        api.migrate(options, function (error) {
            if (error) helper.exit(error);

            console.log('');
            console.log('Done.'.green, 'You can now use your Cloudron at ', String('https://my.' + (options.newFqdn || options.fqdn)).bold);
            console.log('');
        });
    });
}

function listBackups(cloudronDomain, options) {
    assert.strictEqual(typeof cloudronDomain, 'string');
    assert.strictEqual(typeof options, 'object');

    function done(error, result) {
        if (error) helper.exit(error);

        console.log('');

        if (result.length === 0) {
            console.log('No backups have been made.');
            helper.exit();
        }

        result = result.map(function (backup) {
            backup.creationTime = new Date(backup.creationTime);
            return backup;
        }).sort(function (a, b) { return a.creationTime - b.creationTime; });

        var t = new Table();

        result.forEach(function (backup) {
            t.cell('Id', backup.id);
            t.cell('Creation Time', backup.creationTime);
            t.cell('Version', backup.version);

            t.newRow();
        });

        console.log(t.toString());

        helper.exit();
    }

    if (options.provider === 'caas') {
        console.log('Using caas backup listing');
        return caas.getBackupListing(cloudronDomain, {}, done);
    } else if (options.provider === 'ec2') {
        console.log('Using s3 backup listing');
        return ec2.getBackupListing(cloudronDomain, options, done);
    } else if (options.provider === 'digitalocean') {
        return digitalocean.getBackupListing(cloudronDomain, options, done);
    } else if (options.provider) {
        helper.exit('--provider must be either "caas" or "ec2" or "digitalocean"');
    }

    helper.detectCloudronApiEndpoint(cloudronDomain, function (error) {
        if (error) {
            console.error(error);
            helper.exit('Try using the --provider argument');
        }

        helper.getCloudronBackupList(done);
    });
}

function createBackup(cloudron, options) {
    assert.strictEqual(typeof cloudron, 'string');
    assert.strictEqual(typeof options, 'object');

    helper.detectCloudronApiEndpoint(cloudron, function (error) {
        if (error) helper.exit(error);

        function done(error) {
            if (error) helper.exit(error);
            console.log('Backup successful');
        }

        if (options.sshKey) {
            helper.exec('ssh', helper.getSSH(config.apiEndpoint(), options.sshKey, ' curl --fail -X POST http://127.0.0.1:3001/api/v1/backup'), helper.waitForBackupFinish.bind(null, done));
        } else {
            helper.createCloudronBackup(done);
        }
    });
}

function downloadBackup(cloudron, backupFolder, options) {
    assert.strictEqual(typeof cloudron, 'string');
    assert.strictEqual(typeof options, 'object');

    if (!options.backupId) helper.missing('backup-id');

    helper.detectCloudronApiEndpoint(cloudron, function (error) {
        if (error) helper.exit(error);

        helper.downloadBackup(options.backupId, backupFolder || process.cwd(), !!options.decrypt, helper.exit);
    });
}

function eventlog(fqdn, options) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof options, 'object');

    helper.detectCloudronApiEndpoint(fqdn, function (error) {
        if (error) helper.exit(error);

        if (options.sshKey) {

            if (options.full) {
                helper.exec('ssh', helper.getSSH(config.apiEndpoint(), options.sshKey, ' mysql -uroot -ppassword -e "SELECT creationTime,action,source,data FROM box.eventlog ORDER BY creationTime DESC"'));
            } else {
                helper.exec('ssh', helper.getSSH(config.apiEndpoint(), options.sshKey, ' mysql -uroot -ppassword -e "SELECT creationTime,action,source,LEFT(data,50) AS data_preview FROM box.eventlog ORDER BY creationTime DESC"'));
            }

            return;
        }

        helper.superagentEnd(function () {
            return superagent
                .get(helper.createUrl('/api/v1/eventlog'))
                .query({ access_token: config.token() })
                .send({});
        }, function (error, result) {
            if (error) helper.exit(error);
            if (result.statusCode !== 200) return helper.exit(util.format('Failed to fetch eventlog.'.red, result.statusCode, result.text));

            var t = new Table();

            result.body.eventlogs.forEach(function (event) {
                t.cell('creationTime', event.creationTime);
                t.cell('action', event.action);
                t.cell('source', event.source.username || event.source.userId || event.source.ip);
                t.cell('data_preview', options.full ? JSON.stringify(event.data) : JSON.stringify(event.data).slice(-50));

                t.newRow();
            });

            console.log(t.toString());

            helper.exit();
        });
    });
}

function info(fqdn, options) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof options, 'object');

    helper.detectCloudronApiEndpoint(fqdn, function (error) {
        if (error) helper.exit(error);

        var info = null;

        helper.superagentEnd(function () {
            return superagent
                .get(helper.createUrl('/api/v1/cloudron/config'))
                .query({ access_token: config.token() })
                .send({});
        }, function (error, result) {
            if (error) helper.exit(error);
            if (result.statusCode !== 200) return helper.exit(util.format('Failed to fetch info.'.red, result.statusCode, result.text));

            info = result.body;

            helper.superagentEnd(function () {
                return superagent
                    .get(helper.createUrl('/api/v1/settings/backup_config'))
                    .query({ access_token: config.token() })
                    .send({});
            }, function (error, result) {
                if (error) helper.exit(error);
                if (result.statusCode !== 200) return helper.exit(util.format('Failed to fetch backup info.'.red, result.statusCode, result.text));

                info.backupConfig = result.body;

                helper.superagentEnd(function () {
                    return superagent
                        .get(helper.createUrl('/api/v1/settings/dns_config'))
                        .query({ access_token: config.token() })
                        .send({});
                }, function (error, result) {
                    if (error) helper.exit(error);
                    if (result.statusCode !== 200) return helper.exit(util.format('Failed to fetch dns info.'.red, result.statusCode, result.text));

                    info.dnsConfig = result.body;

                    if (options.raw) {
                        console.log(info);
                    } else {
                        console.log('');
                        console.log('Cloudron %s info:', config.fqdn().yellow);
                        console.log('');
                        console.log(' Appstore:     %s', info.apiServerOrigin.cyan);
                        console.log(' Version:      %s', info.version.cyan);
                        console.log(' IP:           %s', info.ip.cyan);
                        console.log(' Provider:     %s', info.provider.cyan);
                        console.log('');

                        console.log('Backup config:');
                        console.log('');
                        Object.keys(info.backupConfig).forEach(function (key) {
                            console.log(' %s: %s %s', key, new Array(20 - key.length).join(' '), info.backupConfig[key].cyan);
                        });
                        console.log('');

                        console.log('DNS config:');
                        console.log('');
                        Object.keys(info.dnsConfig).forEach(function (key) {
                            console.log(' %s: %s %s', key, new Array(20 - key.length).join(' '), info.dnsConfig[key].cyan);
                        });
                        console.log('');
                    }
                });
            });
        });
    });
}

function logs(fqdn, options) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof options, 'object');

    helper.detectCloudronApiEndpoint(fqdn, function (error) {
        var ip = null;

        if (error) {
            if (helper.isIp(fqdn)) {
                ip = fqdn;
            } else {
                helper.exit(error);
            }
        }

        if (!helper.isIp(fqdn)) options.sshKey = options.sshKey || config.sshKey();

        var port = options.sshPort || (config.provider() === 'caas' ? 202 : 22);

        helper.exec('ssh', helper.getSSH(ip || config.apiEndpoint(), options.sshKey, 'journalctl -fa', port), function (error) {
            helper.exit(error);

            // since this is closed with ctrl+c we always get an non-zero exit code
        });
    });
}

function sshLogin(fqdn, cmds, options) {
    assert.strictEqual(typeof fqdn, 'string');
    assert(Array.isArray(cmds));
    assert.strictEqual(typeof options, 'object');

    helper.detectCloudronApiEndpoint(fqdn, function (error) {
        var ip = null;

        if (error) {
            if (helper.isIp(fqdn)) {
                ip = fqdn;
            } else {
                helper.exit(error);
            }
        }

        if (!helper.isIp(fqdn)) options.sshKey = options.sshKey || config.sshKey();

        var port = options.sshPort || (config.provider() === 'caas' ? 202 : 22);

        helper.exec('ssh', helper.getSSH(ip || config.apiEndpoint(), options.sshKey, cmds, port), function (error) {
            if (error) helper.exit(error);

            // preserve option if no ip was passed
            if (!helper.isIp(fqdn)) config.setSshKey(options.sshKey);
        });
    });
}

function hotfix(fqdn, options) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof options, 'object');

    helper.detectCloudronApiEndpoint(fqdn, function (error) {
        var ip = null;
        var code = null;

        if (error) {
            if (helper.isIp(fqdn)) {
                ip = fqdn;
            } else {
                helper.exit(error);
            }
        }

        if (!helper.isIp(fqdn)) options.sshKey = options.sshKey || config.sshKey();

        if (!options.sshKey) helper.missing('ssh-key');
        options.sshKey = helper.findSSHKey(options.sshKey);
        if (!options.sshKey) helper.exit('Unable to find SSH key');

        var port = options.sshPort || (config.provider() === 'caas' ? 202 : 22);

        // required since we mix async and sync code paths
        function doHotfix() {
            var cmds = [
                { cmd: 'systemctl stop cloudron.target' },
                { cmd: 'rm -rf /home/yellowtent/box/* /home/yellowtent/box/.*' },
                { cmd: 'tar zxf - -C /home/yellowtent/box', stdin: fs.createReadStream(code) },
                { cmd: 'cd /home/yellowtent/box && npm rebuild' },
                { cmd: 'chown -R yellowtent.yellowtent /home/yellowtent/box' },
                { cmd: 'sed -e "s/\"restore\"/\"_restore\"/" -i /home/yellowtent/setup_start.sh' }, // do not restore. dangerous.
                { cmd: '/home/yellowtent/setup_start.sh' } // ensure db-migrate runs as well
            ];

            if (options.recreateInfra) {
                console.log('Will recreate infra'.yellow);
                cmds.unshift({ cmd: 'truncate -s 0 /home/yellowtent/data/INFRA_VERSION' });
            }

            helper.sshExec(config.apiEndpoint(), port, options.sshKey, cmds, function (error) {
                if (error) helper.exit(error);

                console.log('Done patching'.green);
            });
        }

        if (options.release) {
            versions.details(options.release, function (error, result) {
                if (error) helper.exit(error);

                console.log('Downloading release locally...');

                code = os.tmpdir() + '/boxtarball.tar.gz';

                var codeStream = fs.createWriteStream(code);
                var req = superagent.get(result.sourceTarballUrl);
                req.pipe(codeStream);

                req.on('error', helper.exit);
                codeStream.on('error', helper.exit);

                req.on('end', doHotfix);
            });
        } else {
            code = options.code;

            if (!code) {
                code = os.tmpdir() + '/boxtarball.tar.gz';
                var tarballScript = path.join(__dirname, 'scripts/createReleaseTarball');
                if (!fs.existsSync(tarballScript)) tarballScript = path.join(__dirname, '../../../box/scripts/createReleaseTarball'); // legacy
                if (!fs.existsSync(tarballScript)) helper.exit('Could not find tarball script. Run this command from the box repo checkout directory.');

                execSync(tarballScript + ' --output ' + code + ' --no-upload --revision ' + (options.revision || 'HEAD'), { stdio: [ null, process.stdout, process.stderr ] });
            }

            doHotfix();
        }
    });
}

function waitForUpdateFinish(callback) {
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Waiting for update to finish...');

    (function checkStatus() {
        superagent.get(helper.createUrl('/api/v1/cloudron/progress')).end(function (error, result) {
            if (error && !error.response) return callback(error);
            if (result.statusCode === 503) return setTimeout(checkStatus, 1000);
            if (result.statusCode !== 200) return callback(new Error(util.format('Failed to get update progress.'.red, result.statusCode, result.text)));
            if (!result.body.update || result.body.update.percent >= 100) return callback();

            process.stdout.write('.');

            setTimeout(checkStatus, 1000);
        });
    })();
}

function update(fqdn, options) {
    assert.strictEqual(typeof fqdn, 'string');
    assert.strictEqual(typeof options, 'object');

    function done(error) {
        if (error) helper.exit(error);

        console.log('');
        console.log('Done.'.green, 'You can now use your Cloudron at ', String('https://' + config.apiEndpoint()).bold);
        console.log('');
    }

    helper.detectCloudronApiEndpoint(fqdn, function (error) {
        if (error) helper.exit(error);

        if (options.sshKey) {
            ssh.updateOrUpgrade(options, function (error) {
                if (error) helper.exit(error);
                waitForUpdateFinish(done);
            });
        } else {
            admin.updateOrUpgrade(options, function (error) {
                if (error) helper.exit(error);
                waitForUpdateFinish(done);
            });
        }
    });
}
