'use strict';

var assert = require('assert'),
    async = require('async'),
    aws = require('./aws.js'),
    config = require('../config.js'),
    dns = require('dns'),
    helper = require('../helper.js'),
    semver = require('semver'),
    ssh = require('./ssh.js'),
    superagent = require('superagent');

exports = module.exports = {
    create: create,
    restore: restore,
    upgrade: upgrade,
    migrate: migrate
};

// those hold output values
var gInstanceId = null;

function checkDNSZone(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    console.log('Checking DNS zone...');

    aws.checkIfDNSZoneExists(params.domain, function (error) {
        if (error) return callback(error);

        callback();
    });
}

function checkS3BucketAccess(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    console.log('Checking S3 bucket access...');

    aws.checkS3BucketAccess(params.backupBucket, function (error) {
        if (error) return callback(error);

        callback();
    });
}

function detectSshKeyName(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Detecting SSH key name on AWS...');

    var fingerprints = helper.getSSHFingerprint(params.sshKey);
    if (fingerprints.length === 0) return callback('Unable to get fingerprint from ssh key');

    aws.findSshKeyName(fingerprints, function (error, keyName) {
        if (error) return callback(error);

        params.sshKeyName = keyName;

        process.stdout.write(keyName + '\n');

        callback();
    });
}

function waitForVPC(vpcId, callback) {
    assert.strictEqual(typeof vpcId, 'string');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Waiting for VPC to be available...');

    async.forever(function (callback) {
        aws.getVPCDetails(vpcId, function (error, result) {
            if (error) return callback(error);
            if (result.State === 'available') return callback('done');

            process.stdout.write('.');

            setTimeout(callback, 1000);
        });
    }, function (doneOrError) {
        if (doneOrError !== 'done') return callback(doneOrError);

        process.stdout.write(vpcId + '\n');

        callback();
    });
}

function waitForSubnet(subnetId, callback) {
    assert.strictEqual(typeof subnetId, 'string');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Waiting for Subnet to be available...');

    async.forever(function (callback) {
        aws.getSubnetDetails(subnetId, function (error, result) {
            if (error) return callback(error);
            if (result.State === 'available') return callback('done');

            process.stdout.write('.');

            setTimeout(callback, 1000);
        });
    }, function (doneOrError) {
        if (doneOrError !== 'done') return callback(doneOrError);

        process.stdout.write(subnetId + '\n');

        callback();
    });
}

function createSubnetAndSecurityGroup(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (params.securityGroup && params.subnet) return callback();

    console.log('Creating VPC...');

    aws.createVPC(function (error, vpcId) {
        if (error) return callback(error);

        waitForVPC(vpcId, function (error) {
            if (error) return callback(error);

            console.log('Creating internet gateway and setup routes...');

            aws.createInternetGateway(vpcId, function (error) {
                if (error) return callback(error);

                console.log('Creating Subnet...');

                aws.createSubnet(vpcId, function (error, subnetId) {
                    if (error) return callback(error);

                    params.subnet = subnetId;

                    waitForSubnet(subnetId, function (error) {
                        if (error) return callback(error);

                        console.log('Creating security group...');

                        aws.createSecurityGroup(vpcId, function (error, securityGroupId) {
                            if (error) return callback(error);

                            params.securityGroup = securityGroupId;

                            callback();
                        });
                    });
                });
            });
        });
    });
}

function createServer(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    console.log('Creating server...');

    ssh.getUserData('ec2', params, function (error, userData) {
        if (error) return callback(error);

        // only set this if version is based on meta data api
        if (semver.lte(params.version, '0.21.1')) params.userData = userData;
        else params.userData = {};

        aws.create(params, function (error, instanceId) {
            if (error) return callback(error);

            gInstanceId = instanceId;

            callback();
        });
    });
}

function waitForServer(callback) {
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Waiting for server to come up...');

    async.forever(function (callback) {
        aws.state(gInstanceId, function (error, state) {
            if (error) return callback(error);
            if (state === 'running') return callback('done');

            process.stdout.write('.');

            setTimeout(callback, 1000);
        });
    }, function (doneOrError) {
        if (doneOrError !== 'done') return callback(doneOrError);

        process.stdout.write('\n');

        callback();
    });

}

function getIp(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    process.stdout.write('Getting public IP...');

    aws.publicIP(gInstanceId, function (error, result) {
        if (error) return callback(error);

        params.publicIP = result;

        console.log(params.publicIP);

        callback();
    });
}

function getInstanceResources(params, callback) {
    assert.strictEqual(typeof params, 'object');
    assert.strictEqual(typeof callback, 'function');

    helper.superagentEnd(function () {
        return superagent.get(helper.createUrl('/api/v1/settings/backup_config')).query({ access_token: config.token() });
    }, function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode !== 200) return callback(new Error('Failed to get instance details.'));

        params.backupKey = result.body.key;
        params.backupBucket = result.body.bucket;

        // FIXME maybe not the best way to get these
        params.awsRegion = result.body.region;
        params.accessKeyId = result.body.accessKeyId;
        params.secretAccessKey = result.body.secretAccessKey;

        aws.init({
            region: params.awsRegion,
            accessKeyId: params.accessKeyId,
            secretAccessKey: params.secretAccessKey
        });

        // do not use params.domain as we need the webadmin origin
        dns.resolve4(config.apiEndpoint(), function (error, addresses) {
            if (error) return callback(error);
            if (!addresses || addresses.length === 0) return callback('Unable to detect Cloudron IP address');

            aws.getInstanceDetails(addresses[0], function (error, result) {
                if (error) return callback(error);
                if (result.State.Name === 'terminated') return callback('Instance is terminated. You may have to use the restore subcommand.');

                params.instanceId = result.InstanceId;
                params.type = result.InstanceType;
                params.subnet = result.SubnetId;
                params.securityGroup = result.SecurityGroups[0].GroupId;

                aws.getVolumeDetails(result.BlockDeviceMappings[0].Ebs.VolumeId, function (error, result) {
                    if (error) return callback(error);

                    params.size = result.Size;

                    callback();
                });
            });
        });
    });
}

// ----------------------------------------------------------------------------
//   Tasks
// ----------------------------------------------------------------------------

function create(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof options.awsRegion, 'string');
    assert.strictEqual(typeof options.version, 'string');
    assert.strictEqual(typeof options.backupKey, 'string');
    assert.strictEqual(typeof options.backupBucket, 'string');
    assert.strictEqual(typeof options.accessKeyId, 'string');
    assert.strictEqual(typeof options.secretAccessKey, 'string');
    assert.strictEqual(typeof options.type, 'string');
    assert.strictEqual(typeof options.size, 'number');
    assert.strictEqual(typeof options.sshKey, 'string');
    assert.strictEqual(typeof options.domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Using version %s', options.version.cyan.bold);

    aws.init({
        region: options.awsRegion,
        accessKeyId: options.accessKeyId,
        secretAccessKey: options.secretAccessKey
    });

    var params = options;

    var tasks = [
        checkDNSZone.bind(null, params),
        checkS3BucketAccess.bind(null, params),
        detectSshKeyName.bind(null, params),
        createSubnetAndSecurityGroup.bind(null, params),
        createServer.bind(null, params),
        waitForServer,
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'ec2', params),
        ssh.uploadUserData.bind(null, 'ec2', params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'ec2', params)
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        console.log('');
        console.log('Cloudron created with:');
        console.log('  ID:        %s', gInstanceId.cyan);
        console.log('  Public IP: %s', params.publicIP.cyan);
        console.log('');

        callback();
    });
}

function restore(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof options.awsRegion, 'string');
    assert.strictEqual(typeof options.backup, 'object');
    assert.strictEqual(typeof options.backupKey, 'string');
    assert.strictEqual(typeof options.backupBucket, 'string');
    assert.strictEqual(typeof options.accessKeyId, 'string');
    assert.strictEqual(typeof options.secretAccessKey, 'string');
    assert.strictEqual(typeof options.type, 'string');
    assert.strictEqual(typeof options.sshKey, 'string');
    assert.strictEqual(typeof options.domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Restoring %s to backup %s with version %s', options.domain.cyan.bold, options.backup.id.cyan.bold, options.backup.version.cyan.bold);

    aws.init({
        region: options.awsRegion,
        accessKeyId: options.accessKeyId,
        secretAccessKey: options.secretAccessKey
    });

    var params = options;

    // use the version of the backup
    params.version = options.backup.version;

    var tasks = [
        checkDNSZone.bind(null, params),
        detectSshKeyName.bind(null, params),
        ssh.getRestoreDetails.bind(null, params),
        createSubnetAndSecurityGroup.bind(null, params),
        createServer.bind(null, params),
        waitForServer,
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'ec2', params),
        ssh.uploadUserData.bind(null, 'ec2', params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'ec2', params)
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        console.log('');
        console.log('Cloudron created with:');
        console.log('  ID:        %s', gInstanceId.cyan);
        console.log('  Public IP: %s', params.publicIP.cyan);
        console.log('');

        callback();
    });
}

function upgrade(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof options.version, 'string');
    assert.strictEqual(typeof options.domain, 'string');
    assert.strictEqual(typeof options.sshKey, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Upgrading %s to version %s...', options.domain.cyan.bold, options.version.cyan.bold);

    var params = options;

    var tasks = [
        ssh.verifySshAccess.bind(null, params),
        getInstanceResources.bind(null, params),
        detectSshKeyName.bind(null, params),
        helper.createCloudronBackup,
        ssh.getLastBackup.bind(null, params),
        ssh.getRestoreDetails.bind(null, params),
        ssh.retireOldCloudron.bind(null, params),
        createServer.bind(null, params),
        waitForServer,
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'ec2', params),
        ssh.uploadUserData.bind(null, 'ec2', params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'ec2', params),
        function (callback) {
            aws.terminateInstance(params.instanceId, callback);
        }
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        console.log('');
        console.log('Cloudron upgraded with:');
        console.log('  ID:        %s', gInstanceId.cyan);
        console.log('  Public IP: %s', params.publicIP.cyan);
        console.log('');

        callback();
    });
}

function migrate(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof options.fqdn, 'string');
    assert.strictEqual(typeof options.sshKey, 'string');
    assert.strictEqual(typeof options.accessKeyId, 'string');
    assert.strictEqual(typeof options.secretAccessKey, 'string');
    assert.strictEqual(typeof callback, 'function');

    console.log('Migrating %s...', options.fqdn.cyan.bold);
    if (options.newFqdn) console.log('  New Domain: %s', options.newFqdn.yellow);
    if (options.size) console.log('  New Volume Size: %s', String(options.size + 'GB').yellow);
    if (options.type) console.log('  New Instance Type: %s', options.type.yellow);

    var params = {
        domain: options.fqdn,
        sshKey: options.sshKey,
        accessKeyId: options.accessKeyId,
        secretAccessKey: options.secretAccessKey
    };

    var tasks = [
        ssh.getInstanceVersion.bind(null, params),
        getInstanceResources.bind(null, params),
        ssh.verifySshAccess.bind(null, params),
        detectSshKeyName.bind(null, params),
        helper.createCloudronBackup,
        ssh.getLastBackup.bind(null, params),
        ssh.getRestoreDetails.bind(null, params),
        function overwriteCloudronDetails(callback) {
            if (options.size) params.size = options.size;
            if (options.type) params.type = options.type;
            if (options.newFqdn) params.domain = options.newFqdn;

            callback(null);
        },
        ssh.retireOldCloudron.bind(null, params),
        createServer.bind(null, params),
        waitForServer,
        getIp.bind(null, params),
        ssh.initBaseSystem.bind(null, 'ec2', params),
        ssh.uploadUserData.bind(null, 'ec2', params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'ec2', params),
        function (callback) {
            aws.terminateInstance(params.instanceId, callback);
        }
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        console.log('');
        console.log('Cloudron created with:');
        console.log('  ID:        %s', gInstanceId.cyan);
        console.log('  Public IP: %s', params.publicIP.cyan);
        console.log('');

        callback();
    });
}
