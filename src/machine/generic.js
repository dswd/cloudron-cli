'use strict';

var assert = require('assert'),
    async = require('async'),
    config = require('../config.js'),
    hat = require('hat'),
    helper = require('../helper.js'),
    ssh = require('./ssh.js');

exports = module.exports = {
    create: create
};

function create(options, version, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof version, 'string');
    assert.strictEqual(typeof callback, 'function');

    if (!options.sshKey) helper.missing('ssh-key');
    if (!options.ip) helper.missing('ip');

    options.sshKey = helper.findSSHKey(options.sshKey);
    if (!options.sshKey) helper.exit('Unable to find SSH key');

    if (!options.backupKey) {
        console.log();
        console.log('No backup key specified.');
        options.backupKey = hat(256);
        console.log('Generated backup key: ', options.backupKey.bold.cyan);
        console.log('The backup key is also stored in the %s config file in your home folder.', 'cloudron.json'.bold.cyan);
        console.log('Remember to keep the backup key stashed. You will need it to restore your Cloudron!'.yellow);
        console.log();
    }

    var params = {
        version: version,
        sshKey: options.sshKey,
        publicIP: options.ip,
        domain: options.fqdn,
        backupKey: options.backupKey
    };

    var tasks = [
        ssh.initBaseSystem.bind(null, 'generic', params),
        ssh.uploadUserData.bind(null, 'generic', params),
        ssh.rebootServer.bind(null, params),
        ssh.waitForDNS.bind(null, params),
        ssh.waitForStatus.bind(null, 'generic', params)
    ];

    async.series(tasks, function (error) {
        if (error) return callback(error);

        // stash for further use
        config.setActive(options.fqdn);
        config.setProvider('generic');
        config.setBackupKey(options.backupKey);
        config.setSshKey(options.sshKey);

        console.log('');
        console.log('Cloudron created with:');
        console.log('  ID:        %s', String(params.instanceId).cyan);
        console.log('  Public IP: %s', options.ip.cyan);
        console.log('');

        callback();
    });
}
