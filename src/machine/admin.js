'use strict';

var assert = require('assert'),
    config = require('../config.js'),
    helper = require('../helper.js'),
    superagent = require('superagent'),
    util = require('util'),
    versions = require('./versions.js');

exports = module.exports = {
    updateOrUpgrade: updateOrUpgrade
};

function updateOrUpgrade(options, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof config.apiEndpoint(), 'string');
    assert.strictEqual(typeof callback, 'function');

    helper.superagentEnd(function () {
        return superagent
            .get(helper.createUrl('/api/v1/cloudron/config'))
            .query({ access_token: config.token() });
    }, options, function (error, result) {
        if (error) return callback(error);
        if (result.statusCode !== 200) return callback(util.format('Failed to get Cloudron configuration.'.red, result.statusCode, result.text));
        if (!result.body.update || !result.body.update.box) {
            console.log('Already on latest version.'.green);
            return helper.exit();
        }

        var boxUpdate = result.body.update.box;

        console.log('New version %s available.', boxUpdate.version.cyan);
        console.log('');
        console.log('Changelog:');
        boxUpdate.changelog.forEach(function (c) { console.log('  * ' + c.bold.white); });
        console.log('');

        versions.details(boxUpdate.version, function (error) {
            if (error) return callback(util.format('Version %s cannot be resolved. Is the specified versions file url correct?', boxUpdate.version));

            console.log('Start update...');

            helper.superagentEnd(function () {
                return superagent
                    .post(helper.createUrl('/api/v1/cloudron/update'))
                    .query({ access_token: config.token() })
                    .send({});
            }, function (error, result) {
                if (error) return callback(error);
                if (result.statusCode === 412) return callback('Automatic upgrades not supported.'.red + ' See https://cloudron.io/references/selfhosting.html#upgrade');
                if (result.statusCode === 409) return callback('Cloudron not ready for update. Retry later.'.red);
                if (result.statusCode !== 202) return callback(util.format('Failed to update Cloudron.'.red, result.statusCode, result.text));

                callback(null);
            });
        });
    });
}
